package com.supakit.editgameox;

import java.util.Scanner;

public class TicTacToe {

    static int count = 0;
    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static char player = 'X';
    static Scanner kb = new Scanner(System.in);

    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };

    static void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");

    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            count++;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
            count--;
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;

    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkX() {
        for (int rowcol = 0; rowcol < 3; rowcol++) {
            if (table[rowcol][rowcol] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;

    }

    static void checkX2() {
        if (table[0][2] == player
                && table[1][1] == player
                && table[2][0] == player) {
            isFinish = true;
            winner = player;
        }

    }

    static void checkDraw() {
        while (count == 9) {
            isFinish = true;
            winner = '-';
            break;
        }

    }

    static void checkWin() {
        checkCol();
        checkRow();
        checkX();
        checkX2();
        checkDraw();

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }

    }

    static void showResult() {
        if (winner == '-') {
            showTable();
            System.out.println("Draw!!!");
        } else {
            showTable();
            System.out.println("Player " + winner + " win....");
        }

    }

    static void showBye() {
        System.out.println("Bye bye ....");

    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

    }

}
